function loadTwitter() {
    if($(".twitter-share-button").length > 0){
        if (typeof (twttr) != 'undefined') {
            twttr.widgets.load();
        } else {
            $.getScript('http://platform.twitter.com/widgets.js');
        }
    }
}

function loadFB(){
	if ($(".fb-like").length > 0) {
    if (typeof (FB) != 'undefined') {
       FB.init({ status: true, cookie: true, xfbml: true });
    } else {
      $.getScript("http://connect.facebook.net/en_US/all.js#xfbml=1", function () {
        FB.init({ status: true, cookie: true, xfbml: true });
      });
    }
	}

}

function loadGooglePlus(){
	var gbuttons = $(".g-plusone.new");
	if (gbuttons.length > 0) {
	  if (typeof (gapi) != 'undefined') {
	      gbuttons.each(function () {
	      		$(this).removeClass("new");
	          gapi.plusone.render($(this).get(0));
	      });
	  } else {
	      $.getScript('https://apis.google.com/js/plusone.js');
	  }
	}
}