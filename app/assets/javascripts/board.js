//= require application
//= require jquery.isotope.js
//= require isotope_theme.js
//= require rendering_isotope_template.js
//= require jquery_plugins/lazy_load.js
//= require_self



var initJqUIComponents = function(){
	var slider  = $("#dh-sidebar #slider"),	tooltip = $('#dh-sidebar .slider-tip');
	tooltip.text('1000')
	slider.slider({
		range: "min",
		min: 5,
		value: 1000,
		max: 1000,
		slide: function(event, ui) {
			var value  = slider.slider('value');
			tooltip.css('left', value/4.35).text(ui.value);
		},
		create: function(event, ui){
			var value  = slider.slider('value');
			tooltip.css('left', value/4.35).text(ui.value);
		}
		});
	
}