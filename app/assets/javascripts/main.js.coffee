# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
//= require application
//= require jquery_ui_widget
//= require jquery_plugins/rs_carousel


$ ->
	$('#my-carousel').carousel(
    itemsPerPage: 1,
    itemsPerTransition: 1,
    noOfRows: 1,
    fade: true,	
    nextPrevLinks: false,
    pagination: true,
    speed: 'normal',
    interval: 7000,
    easing: 'swing');
