if (navigator.userAgent.match(/iPhone/i)) {
	// Fix iPhone viewport scaling bug on orientation change
	// By @mathias, @cheeaun and @jdalton
	( function(doc) {

			var addEvent = 'addEventListener', type = 'gesturestart', qsa = 'querySelectorAll', scales = [1, 1], meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];

			function fix() {
				meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
				doc.removeEventListener(type, fix, true);
			}

			if (( meta = meta[meta.length - 1]) && addEvent in doc) {
				fix();
				scales = [.25, 1.6];
				doc[addEvent](type, fix, true);
			}

		}(document));
}

/////////////////////////////////////////////
// Add :nth-of-type() selector to all browsers
/////////////////////////////////////////////
function getNthIndex(cur, dir) {
	var t = cur, idx = 0;
	while ( cur = cur[dir]) {
		if (t.tagName == cur.tagName) {
			idx++;
		}
	}
	return idx;
}

function isNthOf(elm, pattern, dir) {
	var position = getNthIndex(elm, dir), loop;
	if (pattern == "odd" || pattern == "even") {
		loop = 2;
		position -= !(pattern == "odd");
	} else {
		var nth = pattern.indexOf("n");
		if (nth > -1) {
			loop = parseInt(pattern, 10);
			position -= (parseInt(pattern.substring(nth + 1), 10) || 0) - 1;
		} else {
			loop = position + 1;
			position -= parseInt(pattern, 10) - 1;
		}
	}
	return (loop < 0 ? position <= 0 : position >= 0) && position % loop == 0
}

var pseudos = {
	"first-of-type" : function(elm) {
		return getNthIndex(elm, "previousSibling") == 0;
	},
	"last-of-type" : function(elm) {
		return getNthIndex(elm, "nextSibling") == 0;
	},
	"only-of-type" : function(elm) {
		return pseudos["first-of-type"](elm) && pseudos["last-of-type"](elm);
	},
	"nth-of-type" : function(elm, b, match, all) {
		return isNthOf(elm, match[3], "previousSibling");
	},
	"nth-last-of-type" : function(elm, i, match) {
		return isNthOf(elm, match[3], "nextSibling");
	}
}


/////////////////////////////////////////////
// Fix Header To Be Responsive by normalizing bootstrap
/////////////////////////////////////////////


jQuery(document).ready(function() {
	$("header .container, #h-top-grid .container").addClass("pagewidth").removeClass("container");
	$("header .offset1, #h-top-grid .offset1").removeClass("offset1");
});


/////////////////////////////////////////////
// Page Width Calculation
/////////////////////////////////////////////
var ItemBoard = {
	init : function(config) {
		this.config = config;
		this.bindEvents();
	},
	columns : 0,
	itemMargin : 20,
	itemPadding : 0,
	bindEvents : function() {
		var _self = this;
		// jQuery(document).ready(function() {
		// 	_self.elementSetup()
		// });
		jQuery(window).resize(function() {
			_self.elementSetup()
		});
	},

	elementSetup : function() {
		// var item = jQuery(this.config.itemElement), viewport_width = this.viewportWidth();

		// this.itemWidthOuter = this.itemWidthInner() + this.itemMargin + this.itemPadding;
		// this.columns = parseInt(viewport_width / this.itemWidthOuter);

		// fixwidth = this.columns * this.itemWidthInner() + ((this.columns - 1) * this.itemMargin);
		// maxWidth = '100%';
		viewport_width = this.viewportWidth() - 20;
		// if(viewport_width < 991){
		// 	return false;
		// }

		this.itemWidthOuter = this.itemWidthInner() + this.itemMargin + this.itemPadding;
		// this.columns = parseInt(viewport_width / this.itemWidthOuter);

		// fixwidth = this.columns * this.itemWidthInner() + ((this.columns - 1) * this.itemMargin);
		maxWidth = '100%';
		fixwidth = Math.floor(viewport_width / this.itemWidthOuter) * this.itemWidthOuter;
		// maxWidth = '100%';
		// make exception for width smaller than 480px then dont apply the inline width
		// assume 480 = 1 column item and apply to only viewport <= 505
		// if (this.columns <= 1 && viewport_width <= 505) {
		// 	fixwidth = 978;
		// 	maxWidth = '94%';
		// }

		$('header .span10, #h-top-grid .span10' ).width(fixwidth - 20); // 20 is the most right tile margin
		$('header .span7').width( ((fixwidth - 20) - 270) - 30 ); // 30 is margin. 270 is the span3 width. TODO: Maybe make it responsive too
		$("#h-top-grid ul li").width((fixwidth - 20) / 4);
		// TODO Find any other place for filter. or dont know
		// Make header responsive

		jQuery(this.config.appliedTo).each(function() {
			jQuery(this).css({
				'width' : fixwidth + 'px',
				'max-width' : maxWidth,
			});
		});
		this.fixwidth = fixwidth;
		this.boadWidth = $("#body").outerWidth();
	},

	itemWidthInner : function() {
		var innerwidth = jQuery(this.config.itemElement).width();
		return innerwidth;
	},

	viewportWidth : function() {
		return jQuery(window).width();
	}
};

(function($) {
$.fn.bindWithDelay = function( type, data, fn, timeout, throttle ) {

	if ( $.isFunction( data ) ) {
		throttle = timeout;
		timeout = fn;
		fn = data;
		data = undefined;
	}

	// Allow delayed function to be removed with fn in unbind function
	fn.guid = fn.guid || ($.guid && $.guid++);

	// Bind each separately so that each element has its own delay
	return this.each(function() {
        
        var wait = null;
        
        function cb() {
            var e = $.extend(true, { }, arguments[0]);
            var ctx = this;
            var throttler = function() {
            	wait = null;
            	fn.apply(ctx, [e]);
            };
            
            if (!throttle) { clearTimeout(wait); wait = null; }
            if (!wait) { wait = setTimeout(throttler, timeout); }
        }
        
        cb.guid = fn.guid;
        
        $(this).bind(type, data, cb);
	});


}
})(jQuery);

jQuery(document).ready(function($) {
	
	var isTyping = null;
	var delayCheck = 500;

	
	$.extend($.expr[':'], pseudos);

	/////////////////////////////////////////////
	// HTML5 placeholder fallback
	/////////////////////////////////////////////
	$('[placeholder]').focus(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if (input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});

	/////////////////////////////////////////////
	// Scroll to top
	/////////////////////////////////////////////
	$('.back-top a').click(function() {
		$('body,html').animate({
			scrollTop : 0
		}, 800);
		return false;
	});

	//////////////////////////////////////////////
	////Animate The Side Bar
	/////////////////////////////////////////////
  $("#dh-sidebar").hover(
    function(){
      $('#dh-sidebar').animate({left: 0 }, {duration: 400, easing: 'swing', queue: false});
      $(".header .dh-navigation-bar").animate({marginLeft: 300 }, {duration: 390, easing: 'swing', queue: false, complete: function(){
      	var span = $(this).find('#dh-main-navigation')
      	span.width(span.width() - 242);
      }});
      $("#body #layout #content").animate({marginLeft: 270 }, {duration: 390, easing: 'swing', queue: false, complete: function(){
      	$(this).width($(this).width() - 242)
      	$(window).resize();
      }});
    },
    function(){
    	var margin = ItemBoard.boadWidth - ItemBoard.fixwidth;
      $('#dh-sidebar').animate({left: -300}, {duration: 400, easing:  'swing', queue: false});
      $(".header .dh-navigation-bar").animate({marginLeft: 30}, {duration: 390, easing: 'swing', queue: false, complete: function(){
      		var span = $(this).find('#dh-main-navigation');
      	span.width("auto");
      }});
      $("#body #layout #content").animate({marginLeft: 0}, {duration: 390, easing: 'swing', queue: false, complete: function(){
      	$(this).width("100%");
      	$(window).resize();
      }});
    }
  );

	/////////////////////////////////////////////
	// Prepend zoom icon to prettyphoto
	/////////////////////////////////////////////
	$('.post-image .lightbox').prepend('<span class="zoom"></span>');


});

jQuery(document).ready(function() {
	
	var $container = jQuery('.loops-wrapper');
	$("#body #layout").width(968);
	// auto width
	if( $container.length > 0 ){
		ItemBoard.init({
			itemElement : '.AutoWidthElement .post',
			appliedTo   : '.pagewidth'
		});
	}
	
	// Add social buttons
	addSocialButtons('#content');

	// isotope init
	$container.isotope({
		itemSelector : '.post',
		columnWidth : 242
	});
	$("#body #content").css("visibility", "visible");
	jQuery(window).resize();
	// TODO: fix  height on element (images placeholder) in tiles too, so that they are rendered properly when image take ling to load
	
});

function addSocialButtons($context){
	// if( typeof $context === 'undefined' ) $context = jQuery('#content'); 
	// // Social share
	// if( jQuery('.social-share', $context).length > 0 ){
	// 	jQuery('.twitter-share', $context).sharrre({
	// 		share : {
	// 			twitter : true
	// 		},
	// 		template : themifyScript.sharehtml,
	// 		enableHover : false,
	// 		click : function(api, options) {
	// 			api.simulateClick();
	// 			api.openPopup('twitter');
	// 		}
	// 	});
	// 	jQuery('.facebook-share', $context).sharrre({
	// 		share : {
	// 			facebook : true
	// 		},
	// 		template : themifyScript.sharehtml,
	// 		enableHover : false,
	// 		click : function(api, options) {
	// 			api.simulateClick();
	// 			api.openPopup('facebook');
	// 		}
	// 	});
	// 	jQuery('.pinterest-share', $context).each(function(index, domElem){
	// 		jQuery(this).sharrre({
	// 			share : {
	// 				pinterest : true
	// 			},
	// 			buttons: { 
	// 			   pinterest: {
	// 					url: jQuery(this).attr('data-url'),
	// 					media: jQuery(this).attr('data-media'),
	// 					description: jQuery(this).attr('data-description')
	// 				}
	// 			},
	// 			template : themifyScript.sharehtml,
	// 			enableHover : false,
	// 			urlCurl: themifyScript.sharrrephp,
	// 			click : function(api, options) {
	// 				api.simulateClick();
	// 				api.openPopup('pinterest');
	// 			}
	// 		});
	// 	});
	// 	jQuery('.googleplus-share', $context).sharrre({
	// 		share: {
	// 			googlePlus: true
	// 		},
	// 		template: themifyScript.sharehtml,
	// 		enableHover: false,
	// 		urlCurl: themifyScript.sharrrephp,
	// 		click: function(api, options){
	// 			api.simulateClick();
	// 			api.openPopup('googlePlus');
	// 		}
	// 	});
	// }
}