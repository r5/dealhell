//////////////////////////////////////////////
/// Render Underscore Template First Time
/////////////////////////////////////////////

var MyHell = {
	init: function function_name (argument) {
		this.scrollable();
	},
	disableScrolling: false,
	page: 2,
	isoTemplate: "",
	scrollable: function(){
		$('footer').addClass("gone").addClass("pagewidth"); // Lets hide the footer when there is room for paging
		var _self = this;
		window.scrolling = false;
		$(window).bind("scroll", function() {
			if(window.scrolling || _self.disableScrolling ) return;
			if($(window).scrollTop() + $(window).height() + 250 >= $(document).height()) {
				window.scrolling = true;
				_self.loadResultsForURL({
					removeCurrentElements: false,
					url: window.location.pathname.replace(/\/$/, '') + "/page" + _self.page + "?" + _self.applyFilters(), // strip / from the end of the pathname
					complete: function(){window.scrolling = false;}, success: function(res){ _self.page = _self.page + 1; if(res == 'end'){ _self.disableScrolling = true; $('footer').removeClass("gone").removeClass("pagewidth").css('width', 'auto');} }
				});
			}
		});
	},
	loadResultsForURL: function(config){ // TODO: do remember to cache ajax requests when working on filters
		var _self = this;
		var defaults = {
			data: {}, removeCurrentElements: true , success: function(){},complete: function(){},error: function(){},beforeSend: function(){}, afterInsert: function(){}
		}
		config = $.extend({}, defaults, config )
		$.ajax({
			url: config.url, data: config.data,
			beforeSend: function(){
				config.beforeSend();
				$("#loading-image").show(); // have a loading image at bottom and top
			},
			success: function(res, textStatus, jqXHR){
				config.success(res);
				if(res == 'end'){
					_self.disableScrolling = true;
					return 1;
				}
				
				if(config.removeCurrentElements){
				}
				else{
					var paginatedProducts = JSON.parse(res)
					paginatedProducts = applyTemplate(_self.isoTemplate, paginatedProducts);
					insertToIsotope($(paginatedProducts), function(){});
				}
			},
			complete: function(){ config.complete(); $("#loading-image").hide(); }
		});
	},
	applyFilters: function (argument) {
	}
}

function applyTemplate(template, items){
	var str = "";
	_.each(items, function(item){
		str =  str + template({item: item})
	});
	return removeExtras($(str));
}
function bindLazyLoad($img){
	$img.lazyload({
     event: "scrollstop",
     threshold : 125
 	});
}


function removeExtras($data){
		$data.each(function(i){
			if($data[i] && $data[i].nodeName.toLowerCase() == "#text")
				$data.splice(i,1);
		});
		return $data;
}
function insertToIsotope($data, callback){
	bindLazyLoad($data.find('.post-image img'))
	$(".loops-wrapper").isotope('insert', $data, callback)
}


jQuery(document).ready(function() {
	MyHell.init()
	MyHell.isoTemplate = _.template($("#dh-product-tile").html());
	insertToIsotope(applyTemplate(MyHell.isoTemplate, product_json), function(){});
});


///////////////////////////////////////////////////////////////////
/////////////////Side Bar Actions
///////////////////////////////////////////////////////////////////