jQuery(window).load(function() {
  $.getScript('/assets/jquery_plugins/chosen.js', function(data, textStatus, jqxhr) {
    $(".dh-chosen").chosen();
  });
});

jQuery(document).ready(function() {

  // This is going to init the search and selects in sidebar

  $("#dh-main-navigation .nav-itm").click( function(){
    $("#dh-main-navigation .nav-itm .itm-content.active").removeClass("active");
    $(this).find(".itm-content").addClass("active");
  });

  $(".dh-menu-category-names li").mouseover(function(){
    var $prnt = $(this).parent().parent();
    var $chidrn = $prnt.find("." + $(this).data("children"));
    if($chidrn.is(":visible")){
      return false;
    }
    else{
      $prnt.find(".dh-menu-category-names li.active").removeClass("active");
      $(this).addClass("active");
      $prnt.find(".dh-menu-category-list:visible").hide().removeClass("active");
      $chidrn.show().addClass("active");
    }
  });
});