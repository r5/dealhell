// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
// TODO USE google CDN for jquery
// button.js is used
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require underscore
//= require header
//= require socialscripts

// init all ur jquery UI components here

jQuery(window).load(function() {
	if(typeof(initJqUIComponents) != "undefined") {
	$.getScript('//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js', function(data, textStatus, jqxhr) {
		initJqUIComponents(); // this should only be defined once per layout
	});
}

	var arr = ["/boards/ideeli", "/boards/T-shirt%20hell"]

	_.each($("header a , footer a, #h-top-grid a"), function(s){
		$(s).attr('href', arr[Math.floor((Math.random()*2)+1)	])
	});
	$("header li").click(function(){
		window.location.pathname  = "/boards/ideeli"
	})
});






