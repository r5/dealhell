class BoardController < ApplicationController
	before_filter :find_board, :only => [:index, :pager]

  def index
  	@products = Oj.dump(@board.products.limit(40).map(&:attributes))
  end

  def pager
  	products = @board.products.paginate(:page => params[:page], :limit => 40).map(&:attributes);
  	products = Oj.dump(products) if products.present?
  	render :text => products.present? ? products : 'end'
  end

  private

  def find_board
  	@board = Board.where(name: params[:name]).first || Board.find(2)
  end
end
