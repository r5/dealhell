class Board < ActiveRecord::Base
  attr_accessible :category_id, :description,:caption, :featured, :name, :popularity, :website
  has_many :products
end
