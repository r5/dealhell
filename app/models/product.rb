class Product < ActiveRecord::Base
  attr_accessible :affiliate_url, :board_id, :buyer_count, :caption, :category_id, :description, :detailable, :discount, :image_url, :likes, :list_price, :min_price, :name, :source_id, :thumb_image_url, :valid_till, :views
end
