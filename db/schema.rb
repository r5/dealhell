# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121006184315) do

  create_table "advertisers", :force => true do |t|
    t.string   "name"
    t.integer  "rank"
    t.string   "url"
    t.integer  "source_id"
    t.string   "status"
    t.integer  "connected_through"
    t.integer  "commission"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "source_name"
  end

  create_table "advertisings", :force => true do |t|
    t.integer  "sub_category_id"
    t.integer  "advertiser_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "boards", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "caption"
    t.integer  "category_id"
    t.boolean  "featured"
    t.integer  "popularity"
    t.string   "website"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.string   "status"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "position",   :default => 1
  end

  create_table "filterings", :force => true do |t|
    t.integer  "advertiser_id"
    t.integer  "sub_category_filter_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "sub_category_id"
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.string   "caption"
    t.text     "description"
    t.integer  "list_price"
    t.integer  "min_price"
    t.integer  "discount"
    t.datetime "valid_till"
    t.string   "thumb_image_url"
    t.string   "image_url"
    t.integer  "views"
    t.integer  "likes"
    t.integer  "category_id"
    t.string   "affiliate_url"
    t.integer  "source_id"
    t.integer  "board_id"
    t.integer  "buyer_count"
    t.boolean  "detailable"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "thumb_width"
    t.integer  "thumb_height"
    t.integer  "advertiser_id"
    t.integer  "sub_category_filter_id"
    t.integer  "sub_category_id"
  end

  create_table "sub_categories", :force => true do |t|
    t.string   "name"
    t.string   "cj_name"
    t.integer  "category_id"
    t.string   "status"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "position",    :default => 1
  end

  create_table "sub_category_filters", :force => true do |t|
    t.string   "name"
    t.integer  "sub_category_id"
    t.string   "status"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.integer  "position",        :default => 1
  end

end
