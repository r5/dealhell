class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :caption
      t.text :description
      t.integer :list_price
      t.integer :min_price
      t.integer :discount
      t.datetime :valid_till
      t.string :thumb_image_url
      t.string :image_url
      t.integer :views
      t.integer :likes
      t.integer :category_id
      t.string :affiliate_url
      t.integer :source_id
      t.integer :board_id
      t.integer :buyer_count
      t.boolean :detailable

      t.timestamps
    end
  end
end
