class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.string :name
      t.text :description
      t.string :caption
      t.integer :category_id
      t.boolean :featured
      t.integer :popularity
      t.string :website

      t.timestamps
    end
  end
end
