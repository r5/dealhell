class AddThumbWidthAndThumbHeightToProduct < ActiveRecord::Migration
  def change
    add_column :products, :thumb_width, :integer
    add_column :products, :thumb_height, :integer
  end
end
